# License #

## BASH Labs Private License ##

This software is copyrighted, copy-protected and comes under
BASH Labs's private licensing terms. No copying, modification
or unauthorized viewing of this code is allowed by any means
whatsoever.

Copyright (c) 2016-17 BASH Labs Private Limited, India
