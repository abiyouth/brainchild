<?php

/*
 *  brainchild > OTP.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

include_once "Enums.php";

class OTP
{
    /*
     * One Time Password/Pin helper class
     */

    var $db = NULL;
    var $session = null;

    public function __construct(&$db, &$session)
    {
        $this->db = &$db;
        $this->session = &$session;

    }

    public function Send_OTP_Registration($phone)
    {
        /*
         * Send OTP for registration to phone number
         */

        // Get user details with phone number.
        $user = $this->db->select('users', '*', [
            "phone" => $phone
        ]);

        if(count($user)){

            // Generate random 6 digit number for OTP and store in OTP table.
            $otp = rand(100000,999999);
            $this->db->insert("otp",[
                "user_id" => $user[0]['id'],
                "otp" => $otp
            ]);
            // TODO: Integrate SMS gateway
            // send_otp($phone, $otp);
            return 1;
        }else{
            return 0;
        }

    }

    public function Verify_OTP($phone, $otp)
    {
        /*
         * Verify OTP
         */

        // Get user details with phone number.
        $user = $this->db->select('users', '*', [
            "phone" => $phone
        ]);

        if(count($user)){

            // Remove OTP entry from OTP table
            $check = $this->db->delete("otp", [
                "AND" => [
                    "user_id" => $user[0]['id'],
                    "otp" => $otp
                ]
            ]);
            if($check==1){

                // UPDATE user status to VERIFIED (Allows to register event)
                $this->db->update('users',[
                    "status" => USER_STATUS::$VERIFIED
                ],[
                    "phone" => $phone
                ]);
                return ['status'=>'success', 'message'=>'Phone number verified. Login to register events.'];
            }else{
                return ['status'=>'fail', 'message'=>'No chance :P'];
            }
        }else{
            return ['status'=>'fail', 'message'=>'Invalid PIN.'];
        }
    }

    public function Resend_OTP($phone)
    {
        /*
         * Resend OTP
         */

        // Get user details with phone number.
        $user = $this->db->select('users', '*', [
            "phone" => $phone
        ]);

        if(count($user)){

            $check = $this->db->select('otp', "*", [
                "user_id" => $user[0]['id']
            ]);

            if(count($check)){
                // Generate random 6 digit number for OTP and store in OTP table.
                $otp = rand(100000,999999);

                // UPDATE OTP with new pin.
                $this->db->update('otp',[
                    "otp" => $otp
                ],[
                    "user_id" => $user[0]['id']
                ]);

                // TODO: Integrate SMS gateway
                // send_otp($phone, $otp);
                return ['status'=>'success','message'=>'OTP sent again.'];
            }else{
                return ['status'=>'fail', 'message'=>'Invalid PIN.'];
            }

        }else{
            return ['status'=>'fail', 'message'=>'Invalid PIN.'];
        }

    }
}