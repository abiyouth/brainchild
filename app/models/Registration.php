<?php

/*
 *  brainchild > Registration.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

include_once "Enums.php";
include_once "OTP.php";

class Registration
{
    var $db = null;
    var $session = null;

    public function __construct(&$db, &$session)
    {
        $this->db = &$db;
        $this->session = &$session;
    }

    public function internal($regno, $name, $pass, $phone, $gender, $dob)
    {
        /*
         * Register internal participants
         */

        // Check if valid Karunya id
        $karunya = $this->db->select('karunya',"*", [
            "regno" => $regno
        ]);

        if(count($karunya)){
            $email = $karunya[0]['email'];
        }else{
            return ['status'=>'fail','message'=>'Not a Karunya register number.'];
        }

        // Check if username already exists
        $check = $this->db->count('users', [
            'username' => $regno
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Register number already exists.'];
        }

        // Check if phone number already exists
        $check = $this->db->count('users', [
            'phone' => $phone
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Phone number already exists.'];
        }

        // Insert into registration table
        $this->db->insert('users', [
            "username" => $regno,
            "password" => $pass,
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "dob" => $dob,
            "gender" => $gender,
            "college" => COLLEGE::$HOME,
            "ip" => (string)$_SERVER['REMOTE_ADDR'],
            "type" => USER_TYPE::$STUDENT_INTERNAL,
            "status" => USER_STATUS::$OTP_SENT
        ]);

        $otp = new OTP($this->db, $this->session);
        $check = $otp->Send_OTP_Registration($phone);

        return ['status'=>'success', 'message'=>'Verify OTP.'];

    }

    public function external($regno, $name, $pass, $email, $phone, $college, $gender, $dob)
    {
        /*
         * Register external participants
         */

        // Check if username already exists
        $check = $this->db->count('users', [
            'username' => $regno
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Register number already exists.'];
        }

        // Check if phone number already exists
        $check = $this->db->count('users', [
            'phone' => $phone
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Phone number already exists.'];
        }

        // Check if email already exists
        $check = $this->db->count('users', [
            'email' => $email
        ]);

        if($check>0){
            return ['status'=>'fail', 'message'=>'Email already exists.'];
        }

        // Insert into registration table
        $this->db->insert('users', [
            "username" => $regno,
            "password" => $pass,
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "dob" => $dob,
            "gender" => $gender,
            "college" => $college,
            "ip" => (string)$_SERVER['REMOTE_ADDR'],
            "type" => USER_TYPE::$STUDENT_EXTERNAL,
            "status" => USER_STATUS::$OTP_SENT
        ]);

        $otp = new OTP($this->db, $this->session);
        $check = $otp->Send_OTP_Registration($phone);

        return ['status'=>'success', 'message'=>'Verify OTP.'];

    }


}