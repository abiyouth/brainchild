<?php

/*
 *  brainchild > Event.php
 *  =========================
 *
 *  :copyright: (c) 2016-17 by BASH Labs Private Limited.
 *  :license: BASH Labs Private License. See LICENSE.md for more details.
 */

include_once "Enums.php";

class Event
{
    var $db = NULL;

    public function __construct(&$db)
    {
        $this->db = &$db;
    }

    public function getEvents($dept)
    {
        $result = $this->db->select('events','*',[
            "dept" => $dept
        ]);
        return $result;
    }

    public function getEventsbyType($dept, $type)
    {
        $result = $this->db->select('events','*',[
            'AND' => [
                "dept" => $dept,
                "type" => $type
            ]
        ]);
        return $result;
    }

    public function getGames()
    {
        $result = $this->db->select('events','*',[
            "type" => EVENT_TYPE::$GAMES
        ]);
        return $result;
    }

    public function getWorkshops()
    {
        $result = $this->db->select('events','*',[
            "type" => EVENT_TYPE::$WORKSHOP
        ]);
        return $result;
    }

}