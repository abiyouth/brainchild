-- phpMyAdmin SQL Dump
-- version 4.6.5.2deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 12, 2017 at 02:28 PM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 5.6.29-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bc_mk17`
--

-- --------------------------------------------------------

--
-- Table structure for table `accomodation`
--

CREATE TABLE `accomodation` (
  `id` int(11) NOT NULL,
  `user_id` varchar(30) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `accom_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` varchar(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `contact` varchar(200) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `imgurl` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `contact`, `user_id`, `imgurl`) VALUES
('CSE', 'Computer Science and Technology', 'asd 921341412', 20000, NULL),
('ECE', 'Electronics and Communications', 'asda 9231314', 20001, NULL),
('IT', 'Information Technology', 'asdasd 82983144', 20002, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `rules` text,
  `venue` text,
  `contact` text,
  `prize` int(11) DEFAULT NULL,
  `fees` int(11) DEFAULT NULL,
  `imgurl` text,
  `dept` varchar(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_id` varchar(30) DEFAULT NULL,
  `sms_count` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `rules`, `venue`, `contact`, `prize`, `fees`, `imgurl`, `dept`, `type`, `status`, `user_id`, `sms_count`, `created_time`) VALUES
(1, 'Codezilla', 'coding', 'rules', 'yet to be confirmed', '982314124', 1800, 0, 'http://godslayer/mk/img/1.png', 'CSE', 0, 0, '9999', 2, '2017-01-08 23:00:10'),
(2, 'Codiii', 'sdasd', 'asdasd', 'yet to be confirmed', '929319244', 2000, 0, 'http://godslayer/mk/img/2.png', 'IT', 0, 0, '10000', 2, '2017-01-08 23:00:10'),
(3, 'Circuits Fun', 'asdad', 'dasdfafasf', 'yet to be confirmed', '932313123', 1000, 0, 'http://godslayer/mk/img/3.png', 'ECE', 0, 0, '10001', 2, '2017-01-08 23:00:10');

-- --------------------------------------------------------

--
-- Table structure for table `event_registration`
--

CREATE TABLE `event_registration` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `registration_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `feedback` text,
  `stars` int(11) DEFAULT NULL,
  `harmony_msg` text,
  `fb_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `game_registration`
--

CREATE TABLE `game_registration` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `turns` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `registration_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `karunya`
--

CREATE TABLE `karunya` (
  `id` int(11) NOT NULL,
  `regno` varchar(12) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karunya`
--

INSERT INTO `karunya` (`id`, `regno`, `name`, `email`) VALUES
(1, 'UL13CS014', 'G Abiyouth', 'abiyouth@karunya.edu.in'),
(2, 'UR13CS015', 'Amith Raji', 'amithraji@karunya.edu.in');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `msg` text,
  `status` int(11) DEFAULT NULL,
  `not_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `otp` int(11) NOT NULL,
  `otp_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp`
--

INSERT INTO `otp` (`id`, `user_id`, `otp`, `otp_time`) VALUES
(1, 25, 271608, '2017-01-06 23:49:22'),
(2, 26, 111190, '2017-01-07 03:32:40');

-- --------------------------------------------------------

--
-- Table structure for table `prize`
--

CREATE TABLE `prize` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `place` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `win_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(6) NOT NULL,
  `college` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `registration_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `phone`, `dob`, `gender`, `college`, `status`, `ip`, `registration_time`, `type`) VALUES
(1, 'admin', '123', 'admin', 'admin@mk.com', '9600424320', '2017-01-06', 'M', 'KU', 0, '127.0.0.1', '2017-01-06 01:00:36', 99),
(24, 'UR13CS015', 'iamyouth', 'Amith', 'amithraji@karunya.edu.in', '9600424323', '1994-04-17', 'M', 'Karunya University', 2, '127.0.0.1', '2017-01-06 03:17:03', 2),
(25, 'UL13CS014', 'iamyouth', 'Abiyouth', 'abiyouth@karunya.edu.in', '9600424321', '1994-04-17', 'M', 'Karunya University', 1, '127.0.0.1', '2017-01-06 23:49:22', 2),
(26, 'fallen', 'iamyouth', 'Godslayer', 'abiyouth@live.co', '9600424322', '1994-04-17', 'M', 'MERIT', 1, '127.0.0.1', '2017-01-07 03:32:39', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accomodation`
--
ALTER TABLE `accomodation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_registration`
--
ALTER TABLE `event_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_registration`
--
ALTER TABLE `game_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karunya`
--
ALTER TABLE `karunya`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `karunya_regno_uindex` (`regno`),
  ADD UNIQUE KEY `karunya_email_uindex` (`email`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp`
--
ALTER TABLE `otp`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `otp_username_uindex` (`user_id`);

--
-- Indexes for table `prize`
--
ALTER TABLE `prize`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `registrations_phone_uindex` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accomodation`
--
ALTER TABLE `accomodation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `event_registration`
--
ALTER TABLE `event_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `game_registration`
--
ALTER TABLE `game_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `karunya`
--
ALTER TABLE `karunya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otp`
--
ALTER TABLE `otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prize`
--
ALTER TABLE `prize`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
